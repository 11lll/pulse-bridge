The orchestration node must satisfy the following dependencies:
Python 2 (v2.6-v2.7)/Python3 (v3.5+)Ansible v2.3+ (on Ubuntu based systems it could be installed by apt-get install ansible )Git
The target must have a functional Ubuntu 16.04 or 18.04 launched.

Another recommendation is to configure a remote service to collect the oracle logs. For example, https://papertrailapp.com/ could be used for this purposes - after registration it will provide log server domain name and port that will be used for the oracle configuration.

1. Generate a pair of SSH keys that will be used by the orchestration node to remotely login to the target node. The generated public key must be added to .ssh/authorized_keys on the target node in the home directory of the user (usually root or ubuntu) that will be configured to perform deployment actions.
run command in terminal: ssh-keygen -t rsa
in AWS in EC2 under "Network & Security" - Key Pairs: add new key - copy to content of the key you created (/home/user/.shh) - use show hidden folders.

2. Clone repository: git clone https://gitlab.com/craftyBernok/pulse-bridge
   cd tokenbridge/deployment

3. In hosts.yml file:
Replace all variables templated with tags (<>) with actual values. - (replace tags aswell)
If you have setup https://papertrailapp.com/ account - uncomment syslog_server_port and enter your IP and port.

4. In file group_vars/pulse.yml change COMMON_FOREIGN_RPC_URL to yours from Infura (Ropsten).

5. To deploy the node run command (change <privkey.file> to your file name id_rsa.pub by default)
ansible-playbook --private-key=~/.ssh/<privkey.file> -i hosts.yml site.yml